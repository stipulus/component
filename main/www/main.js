function init() {
	var ordersPage = new OrdersPage(orders);
	$('body').html(ordersPage.$elem);
}

var orders = [
	{
		name:'Card Table',
		price:15.99
	},
	{
		name:'Lamp',
		price:8.99
	}
];