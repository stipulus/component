var OrdersPage = Component.prototype.extend({
    url:'pages/orders.html',
    className: 'orders',
    orderComponents: null,
    construct: function (orders) {
        var orderPage = this;
        this.orderComponents = new Array();
        orders.forEach(function (orderData,i) {
            var order = new Order(orderData);
            orderPage.orderComponents.push(order);
        });
    },
    init: function (hurdle) {
        hurdle.set();
        setTimeout(function () {
            //wait for action to complete; component will not
            //render until each hurdle "set" is "ready"
            hurdle.complete();
        },200);
    },
    ready: function () {
        //render complete
        var $ul = this.$elem.find('ul');
        this.orderComponents.forEach(function (order,i) {
            $ul.append(order.$elem);
        });
    }
});

var Order = Component.prototype.extend({
    html:'<li>{{name}}<span>${{price}}</span></li>',
    className:'order',
    hurdle:null,
    construct: function (orderData) {
        //component uses mustach to apply data in this.template
        //to the html
        this.template = orderData;
    },
    init: function (hurdle) {

    },
    ready: function () {
        var order = this;
        this.$elem.click(function () {
            console.log(order.template);
        });
    }
});