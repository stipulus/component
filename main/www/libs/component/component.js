/*
component v0.1.1 (alpha)
Copyright (c) 2014, Stipulus Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/
if(typeof document === 'undefined') {
    //Nodejs
    var oopi = require('oopi');
    var $ = function (str) {
        return str;
    }
    $.addClass = function () {};
    $.get = function () {
        return {
            then: function () {

            }
        };
    };
}
var Component = (function () {
    var Hurdle = oopi.class({
        i:null,
        callback: null,
        construct: function (callback) {
            this.i = 1;
            if(typeof callback === 'function') this.callback = callback;
        },
        set: function (num) {
            var num = num || 1;
            this.i+=num;
        },
        complete: function (options) {
            this.i--;
            if(this.i <= 0 && typeof this.callback === 'function') {
                this.callback(options);
                this.i = 0;
            }
        }
    });
    //angular directive like behavior
    var Component = oopi.class({
        html:null,
        url:null,
        tagName:null,
        className:null,
        wrap: false,
        $elem:null,
        useThread:false,
        hurdle:null,
        template: {},
        construct: function (parentHurdle) {
            var component = this;
            this.hurdle = new Hurdle(function () {
                component.applyTemplate();
                if(parentHurdle && parentHurdle instanceof Hurdle) parentHurdle.complete();
            });
            if(typeof this.tagName === 'string') this.$elem = $(this.tagName);
            else this.$elem = $('<div></div>');
            if(this.html) {
                setTimeout(function () {
                    //make async
                    if(typeof component.init === 'function')
                        component.init(component.hurdle);
                    component.hurdle.complete();
                },0);
            } else {
                $.get(this.url).then(function (html) {
                    component.html = html;
                    if(typeof component.init === 'function')
                        component.init(component.hurdle);
                    component.hurdle.complete();
                });
            }
        },
        applyTemplate: function () {
            //async option
            try {
                Mustache.parse(this.html);
                var str = Mustache.render(this.html,this.template);
                if(this.wrap) str = '<div class="'+((this.className)?this.className:"")+'">'+str+'</div>';
                var $elem = $(str);
                //console.log('rendered html',this.html,this.template,str,$elem);
                if(this.$elem.replaceWith)
                    this.$elem.replaceWith($elem);
                this.$elem = $elem;
                if(typeof this.className === 'string') this.$elem.addClass(this.className);
            } catch (e) {
                console.error('Could not render template with data.',e);
                return false;
            }
            if(typeof this.ready === 'function') this.ready();
            return true;
        },
        init: function () {

        },
        add: function (component) {
            this.$elem.parent().append(component.$elem);
        }
    });
    if(typeof exports !== 'undefined') {
        exports.prototype = Component.prototype;
    }
    return Component;
})();