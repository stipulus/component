#!/bin/bash

# deploy to remote server
# copy project to remote
# for load balanced shutdown first -> copy files -> start first -> shutdown second -> copy files -> etc.

echo "Start copy to remote servers..."
copyProjectFiles() {
	ssh -i ~/.ssh/dev.pem $1 'bash -s' < stop.sh
	scp -i ~/.ssh/dev.pem main/* $1:~/main/
	#scp -i ~/.ssh/dev.pem -r main/www/* $1:~/main/www/
	scp -i ~/.ssh/dev.pem main/www/* $1:~/main/www/
	scp -i ~/.ssh/dev.pem main/www/pages/* $1:~/main/www/pages/
	ssh -i ~/.ssh/dev.pem $1 'bash -s' < run.sh
}


#copyProjectFiles user@host

echo "Complete"